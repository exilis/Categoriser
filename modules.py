def str_to_list(string):
    """Converts list-like string to list by removing unneeded charaters and returning list."""
    return string.replace('[', '').replace(']', '').replace("'", "").replace('"', '').split(', ')


def directory_check(dir_path, dir_name):
    """Check if directory exists in given path and if not create one"""
    import os
    if os.path.exists(os.path.join(dir_path, dir_name)):
        pass
    else:
        os.mkdir(dir_name) 


def dest_path(dest_path, dest_dir_name):
    """Provide complete destination path."""
    import os
    return os.path.join(dest_path, dest_dir_name)