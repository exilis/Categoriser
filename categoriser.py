import configparser
import os
from modules import *


# Get extensions from extension.ini configuration file
config = configparser.ConfigParser()
config.read('extensions.ini')
extensions = config['Extensions']


# Get path from user where categorisation will take place
print("Type directory you want to make change in")
while True:
    path = input('path: ')
    if os.path.exists(path) == True:
        break
    else:
        print('This directory doesn\'t exist.')
        continue
os.chdir(path)


# Preparing categories
# Get main categories and extensions assigned to them in string form
categories = {}
for key in config['Extensions']:
    categories[key] = config['Extensions'].get(key)
    
# Convert list-like strings to lists
for key in categories:
    categories[key] = str_to_list(categories[key])
    # categories[key] = tuple(categories.values())

# Uppercase extensions
for key in categories:
    listU = [value.upper() for value in categories[key]]
    categories[key].extend(listU)

# Change lists to tuples for later use
for key in categories:
    categories[key] = tuple(categories[key])


to_categorise = []
exclude_dirs = ['Categorised directories']
categ_dirs = 'Categorised directories'

# Remove category-folders from categorising
for key in categories:
    exclude_dirs.append(key)

# Save only existent files and directories for later categorisation 
for file in os.listdir(path):
    to_categorise.append(file)

# Removes bug where category directories were categorised wrongly
for item in exclude_dirs:
    if item in to_categorise:
        to_categorise.remove(item)

# Separate directories from files
for file in to_categorise:
    if os.path.isdir(file):
        directory_check(path, categ_dirs)
        destination_path = dest_path(path, categ_dirs)

        from_file = os.path.join(path, file)
        to_file = os.path.join(destination_path, file)
        os.rename(from_file, to_file)
        to_categorise.remove(file)
        continue
    else:
        pass

# Categorise remaining files
while(len(to_categorise) != 0):
    for file in to_categorise:
        if os.path.isfile(file):
            for key in categories:
                extension = categories[key]
                if file.endswith(extension):
                    directory_check(path, key)
                    destination_path = dest_path(path, key)

                    from_file = os.path.join(path, file)
                    to_file = os.path.join(destination_path, file)
                    os.rename(from_file, to_file)
                    to_categorise.remove(file)
                else:
                    continue 
        else: 
            pass